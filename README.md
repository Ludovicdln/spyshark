# Spyshark ~ ( Man In The Middle )

<div style="text-align:center"><p align="center"><img src="https://zupimages.net/up/20/46/2qyf.png"></img></p></div>

### It's used to analyze frame for <i>DOFUS</i> with universal IO but you can override methods for any support like <strong><i>IPC</i></strong> or <strong><i>Video Games</i></strong>

<div style="text-align:center"><p align="center"><img src="https://zupimages.net/up/20/46/113x.png"></img></p></div>

```csharp

using System.IO.Pipelines;
using System.Buffers;

namespace Spyshark.src.Network.Sockets.IO
{
    public interface IFrameParser
    {
        bool TryParseFrame(ReadOnlySequence<byte> buffer, out SequencePosition consumed, out IFrame frame);
        ValueTask WriteFrame(IFrame frame, PipeWriter writer);
    }
}

```

```csharp

public abstract class DofusFrameParser : IFrameParser

public virtual bool TryParseFrame(ReadOnlySequence<byte> buffer, out SequencePosition consumed, out IFrame frame)
{
	// look code in src to show processing
}

public virtual async ValueTask WriteFrame(IFrame frame, PipeWriter writer)
{
	// look code in src to show processing
}

```

Based on:

- [`System.IO.Pipelines`](https://github.com/dotnet/corefx/tree/master/src/System.IO.Pipelines) - used to read & write frame
- [`Bedrock Framework`](https://github.com/davidfowl/BedrockFramework/tree/fe439694c193db13708235ce654f3a51f9b7962b/src/Bedrock.Framework/Protocols) - inspired for protocols


Key APIs:

- [`SocketConnection`](https://github.com/mgravell/Pipelines.Sockets.Unofficial/blob/master/src/Pipelines.Sockets.Unofficial/SocketConnection.cs) - transform Socket as Pipe

