﻿using Pipelines.Sockets.Unofficial;
using Spyshark.src.Handlers;
using Spyshark.src.Network;
using Spyshark.src.Network.Sockets;
using Spyshark.src.Network.Sockets.IO;
using Spyshark.src.Services;
using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network
{
    public class MITM
    {
        private Client _client;
        private Client _server;
        private IFrameHandler _clientHandler;
        private IFrameHandler _serverHandler;
        private ServiceLogger _service = new ServiceLogger(); // change it with depedency injection in host builder

        public MITM(Client client, Client server, IFrameHandler clientHandler, IFrameHandler serverHandler)
               => (_client, _server, _clientHandler, _serverHandler) = (client, server, clientHandler, serverHandler);
        public async Task Run()
        { 
            async Task ReadLoopServer()
            {
                try
                {
                    await foreach (var frame in _server.ReadFramesAsync())
                    {
                        _service.GetLogger().LogInformation($"RCV ~ SERVER Frame Id {frame.Id} Length {frame.Payload.Length}");

                        bool result = await _serverHandler.Handle(_client, frame);

                        if (!result)
                        {
                            _client.Disconnect();
                            _service.GetLogger().LogInformation($"~ CLIENT ~ Disconnected");

                            _server.Disconnect();
                            _service.GetLogger().LogInformation($"~ SERVER ~ Disconnected");

                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }

            async Task ReadLoopClient()
            {
                try
                {
                    await foreach (var frame in _client.ReadFramesAsync())
                    {
                        _service.GetLogger().LogInformation($"RCV ~ CLIENT Frame Id {frame.Id} Length {frame.Payload.Length}");

                        await _clientHandler.Handle(_server, frame);
                    }
                }
                catch (Exception e)
                {
                }
             
            }

            Task loopServer = ReadLoopServer();

            Task loopClient = ReadLoopClient();

            await Task.WhenAll(loopServer, loopClient);
        }
    }
}
