﻿using Pipelines.Sockets.Unofficial;
using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network.Sockets
{
    public class WrappedSocket : IListener
    {
        private Socket _socket;
        public WrappedSocket(Socket socket) => _socket = socket;
        public async Task<IDuplexPipe> AcceptAsync() => SocketConnection.Create(await _socket.AcceptAsync());
        public async Task<IDuplexPipe> ConnectAsync(EndPoint endPoint) => await SocketConnection.ConnectAsync(endPoint);
    }
}
