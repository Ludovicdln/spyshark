﻿using Spyshark.src.Protocol.Dofus;
using Spyshark.src.Protocol.Frames;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO.Pipelines;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network.Sockets.IO
{
    public class ClientFrame : DofusFrameParser
    {    
        public ClientFrame() { }
        protected override void ReadHeader(ref SequenceReader<byte> reader, out int frameId, out short lenType)
        {
            base.ReadHeader(ref reader, out frameId, out lenType);
            //reader.TryReadBigEndian(out int instanceId); // use for current version of DOFUS
        }
    }
}
