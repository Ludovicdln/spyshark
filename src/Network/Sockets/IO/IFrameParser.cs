﻿using Spyshark.src.Protocol.Frames;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network.Sockets.IO
{
    public interface IFrameParser
    {
        bool TryParseFrame(ReadOnlySequence<byte> buffer, out SequencePosition consumed, out IFrame frame);
        ValueTask WriteFrame(IFrame frame, PipeWriter writer);
    }
}
