﻿using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network.Sockets
{
    public interface IListener
    {
        Task<IDuplexPipe> AcceptAsync();
        Task<IDuplexPipe> ConnectAsync(EndPoint endPoint);
    }
}
