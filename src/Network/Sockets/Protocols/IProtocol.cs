﻿using Spyshark.src.Protocol.Frames;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network.Sockets.Protocols
{
    public interface IProtocol<T> where T : IFrame
    {
        ValueTask WriteFrame(in T frame);
        IAsyncEnumerable<T> ReadFramesAsync();
    }
}
