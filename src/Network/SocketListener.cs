﻿using Spyshark.src.Handlers;
using Spyshark.src.Network.Sockets;
using Spyshark.src.Network.Sockets.IO;
using Spyshark.src.Network.Sockets.Protocols;
using Spyshark.src.Protocol.Frames;
using Spyshark.src.Services;
using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network
{
    public class SocketListener
    {
        private IListener _server;
        private IPEndPoint _endPoint;
        private ServiceLogger _service = new ServiceLogger();
        public SocketListener(IListener server, IPEndPoint endPoint) => (_server, _endPoint) = (server, endPoint);

        public async Task Run()
        {
            _service.GetLogger().LogInformation("Waiting connection...");

            IDuplexPipe clientPipe = await _server.AcceptAsync();

            _service.GetLogger().LogWarning("RCV ~ CLIENT connected");

            Client client = new Client(clientPipe, new ClientFrame());

            IDuplexPipe serverPipe = await _server.ConnectAsync(_endPoint);

            Client server = new Client(serverPipe, new ServerFrame());

            _service.GetLogger().LogWarning($"~ MITM ~ connected to {_endPoint.Address} : {_endPoint.Port}");

            MITM bridge = new MITM(client, server, new ServerHandler(), new ClientHandler());

            await bridge.Run();          
        }
    }
}
