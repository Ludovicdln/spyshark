﻿using Spyshark.src.Network.Sockets;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Spyshark.src.Network
{
    public class ListenerManager
    {
        public static SocketListener Create(IPEndPoint localPoint, IPEndPoint endPoint)
        {
            Socket sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            sck.Bind(localPoint);
            sck.Listen(100);

            return new SocketListener(new WrappedSocket(sck), endPoint);
        }
    }
}
