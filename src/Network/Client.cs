﻿using Microsoft.Extensions.DependencyInjection;
using Spyshark.src.Network.Sockets.IO;
using Spyshark.src.Network.Sockets.Protocols;
using Spyshark.src.Protocol.Frames;
using Spyshark.src.Services;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Network
{
    //https://docs.microsoft.com/fr-fr/dotnet/standard/io/pipelines#read-a-single-message
    public class Client : IProtocol<IFrame>
    {
        private IDuplexPipe _pipe;
        private IFrameParser _parser;
        public Client(IDuplexPipe pipe, IFrameParser parser) => (_pipe, _parser) = (pipe, parser);
        public virtual async IAsyncEnumerable<IFrame> ReadFramesAsync()
        {
            while (true)
            {
                ReadResult result = await _pipe.Input.ReadAsync();

                if (result.IsCanceled) break;

                ReadOnlySequence<byte> buffer = result.Buffer;

                try
                {
                    while (_parser.TryParseFrame(buffer, out SequencePosition consumed, out IFrame frame))
                    {
                        yield return frame;

                        buffer = buffer.Slice(consumed);
                    }
                }
                finally
                {
                    _pipe.Input.AdvanceTo(buffer.Start, buffer.End);
                }
            }
        }

        public ValueTask WriteFrame(in IFrame frame)
        {
            static async ValueTask Wait(ValueTask task) => await task.ConfigureAwait(false);

            var task = _parser.WriteFrame(frame, _pipe.Output);

            return task.IsCompletedSuccessfully ? default : Wait(task);
        }
        public void Disconnect()
        {
            _pipe.Input.Complete();

            _pipe.Output.Complete();
        }
    }
}
