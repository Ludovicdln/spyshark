﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Protocol.Frames
{
    public class Frame : IFrame
    {
        public uint Id { get; set; }
        public byte[] Payload { get; set; }
        public Frame(uint id, byte[] payload)
        {
            this.Id = id;
            this.Payload = payload;
        }
    }
}
