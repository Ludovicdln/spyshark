﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Protocol.Frames
{
    public interface IFrame
    {
        uint Id { get; set; }
        byte[] Payload { get; set; }
    }
}
