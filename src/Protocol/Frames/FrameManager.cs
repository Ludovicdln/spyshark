﻿using Spyshark.src.Protocol.Core.IO.Serializer;
using Spyshark.src.Protocol.Encoder.IO.Deserializer;
using Spyshark.src.Protocol.Encoder.IO.Serializer;
using Spyshark.src.Protocol.IO;
using Spyshark.src.Protocol.Messages;
using Spyshark.src.Protocol.Messages.Servers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Protocol.Frames
{
    public class FrameManager<T> where T : IMessage
    {
        public static IFrame Create(T message)
        {
            IOManager<T>.Serialize(message, out IEncoder encoder);

            return new Frame(message.Id, encoder.Data);
        }

        public static IFrame ModifyFrameForWorld(IFrame frame)
        {
            SelectedServerDataMessage selectedServerData = new SelectedServerDataMessage();

            IOManager<SelectedServerDataMessage>.Deserialize(selectedServerData, frame.Payload);

            selectedServerData.Address = "127.0.0.1";

            selectedServerData.Port = 8001;

            return FrameManager<SelectedServerDataMessage>.Create(selectedServerData);
        }
    }
}
