﻿using Spyshark.src.Network;
using Spyshark.src.Network.Sockets.IO;
using Spyshark.src.Protocol.Frames;
using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Protocol.Dofus
{
    public abstract class DofusFrameParser : IFrameParser
    {
        private const byte BIT_MASK = 3;

        private const byte BIT_RIGHT_SHIFT_LEN_PACKET_ID = 2;
        public virtual bool TryParseFrame(ReadOnlySequence<byte> buffer, out SequencePosition consumed, out IFrame frame)
        {
            var reader = new SequenceReader<byte>(buffer);

            if (reader.Remaining < BIT_RIGHT_SHIFT_LEN_PACKET_ID)
            {
                frame = default;
                consumed = default;
                return false;
            }

            return TryReadNLength(ref reader, out frame, out consumed);
        }
        public virtual async ValueTask WriteFrame(IFrame frame, PipeWriter writer)
        {
            uint lenType = ReadLenType((uint)frame.Payload.Length);

            int size = (int)(2 + lenType + frame.Payload.Length);

            Memory<byte> memory = writer.GetMemory(size);
      
            this.WriteHeader(memory, frame, lenType);

            memory = memory.Slice(2);

            switch (lenType)
            {
                case 1:
                    memory.Span[0] = (byte)frame.Payload.Length;
                    break;

                case 2:
                    BinaryPrimitives.WriteInt16BigEndian(memory.Span, (short)frame.Payload.Length);
                    break;

                case 3:
                    memory.Span[0] = (byte)(frame.Payload.Length >> 16 & 255);
                    BinaryPrimitives.WriteInt16BigEndian(memory.Span.Slice(1), (short)(frame.Payload.Length & 65535));
                    break;
            }

            frame.Payload.CopyTo(memory.Slice((int)lenType));

            writer.Advance(size);

            await writer.FlushAsync();
        }

        protected virtual bool TryReadNLength(ref SequenceReader<byte> reader,  out IFrame frame, out SequencePosition consumed)
        {
            this.ReadHeader(ref reader, out int frameId, out short lenType);

            uint length = this.ReadLength(lenType, ref reader);

            if (reader.Remaining < lenType || reader.Remaining < length)
            {
                frame = default;
                consumed = default;
                return false;
            }

            frame = new Frame((uint)frameId, reader.Sequence.Slice(reader.Position, length).ToArray());

            reader.Advance(length);

            consumed = reader.Position;

            return true;
        }
        protected virtual void ReadHeader(ref SequenceReader<byte> reader, out int frameId, out short lenType)
        {
            reader.TryReadBigEndian(out short header);

            frameId = (header >> BIT_RIGHT_SHIFT_LEN_PACKET_ID);

            lenType = (short)(header & BIT_MASK);
        }
        protected virtual void WriteHeader(Memory<byte> memory, IFrame frame, uint lenType)
        {
            BinaryPrimitives.TryWriteInt16BigEndian(memory.Span, (short)SubComputeStaticHeader(frame.Id, lenType));      
        }
        private uint ReadLength(short lenType, ref SequenceReader<byte> reader)
        {
            switch (lenType)
            {
                case 1:
                    reader.TryRead(out byte byteLength);
                    return byteLength;

                case 2:
                    reader.TryReadBigEndian(out short shortLength);
                    return (ushort)shortLength;

                case 3:
                    reader.TryRead(out byte byteLength1);
                    reader.TryRead(out byte byteLength2);
                    reader.TryRead(out byte byteLength3);
                    return (uint)(((byteLength1 & 255) << 16) + ((byteLength2 & 255) << 8) + (byteLength3 & 255));

                default:
                    return 0;
            }
        }

        private uint ReadLenType(uint lenght)
        {
            if (lenght > 65535)
            {
                return 3;
            }
            if (lenght > 255)
            {
                return 2;
            }
            if (lenght > 0)
            {
                return 1;
            }
            return 0;
        }

        private uint SubComputeStaticHeader(uint msgId, uint typeLen)
        {
            return msgId << BIT_RIGHT_SHIFT_LEN_PACKET_ID | typeLen;
        }
    }
}
