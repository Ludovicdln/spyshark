﻿using Spyshark.src.Protocol.Encoder.IO.Deserializer;
using Spyshark.src.Protocol.Encoder.IO.Serializer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Spyshark.src.Protocol.Messages.Servers
{
    public class SelectedServerDataMessage : IMessage
    {
        public uint Id => 42u;
        public short ServerId { get; set; }
        public string Address { get; set; }
        public ushort Port { get; set; }
        public bool CanCreateNewCharacter { get; set; }
        public string Ticket { get; set; }
        public SelectedServerDataMessage(short serverId, string address, ushort port, bool canCreateNewCharacter, string ticket) 
            => (ServerId, Address, Port, CanCreateNewCharacter, Ticket) = (serverId, address, port, canCreateNewCharacter, ticket);

        public SelectedServerDataMessage() { }

        public void Serialize(IEncoder encoder)
        {
            encoder.WriteShort(ServerId);
            encoder.WriteString(Address);
            encoder.WriteUShort(Port);
            encoder.WriteBoolean(CanCreateNewCharacter);
            encoder.WriteString(Ticket);
        }

        public void Deserialize(IDecoder decoder)
        {
            ServerId = decoder.ReadShort();
            Address = decoder.ReadString();
            Port = decoder.ReadUShort();
            CanCreateNewCharacter = decoder.ReadBoolean();
            Ticket = decoder.ReadString();
        }
    }
}
