﻿using Spyshark.src.Protocol.Encoder.IO.Deserializer;
using Spyshark.src.Protocol.Encoder.IO.Serializer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Protocol.Messages
{
    public interface IMessage
    {
        uint Id { get; }
        void Serialize(IEncoder encoder);
        void Deserialize(IDecoder decoder);
    }
}
