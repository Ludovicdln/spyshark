﻿using Spyshark.src.Protocol.Encoder.IO.Serializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Spyshark.src.Protocol.Core.IO.Serializer
{
    public class BinaryEncoder : IEncoder
    {
        private BinaryWriter _writer;
        public Stream BaseStream
        {
            get
            {
                return this._writer.BaseStream;
            }
        }
        public long BytesAvailable
        {
            get
            {
                return this._writer.BaseStream.Length - this._writer.BaseStream.Position;
            }
        }
        public long Position
        {
            get
            {
                return this._writer.BaseStream.Position;
            }
            set
            {
                this._writer.BaseStream.Position = value;
            }
        }
        public byte[] Data
        {
            get
            {
                long position = this._writer.BaseStream.Position;
                byte[] array = new byte[this._writer.BaseStream.Length];
                this._writer.BaseStream.Position = 0L;
                this._writer.BaseStream.Read(array, 0, (int)this._writer.BaseStream.Length);
                this._writer.BaseStream.Position = position;
                return array;
            }
        }

        public BinaryEncoder()
        {
            this._writer = new BinaryWriter(new MemoryStream(), Encoding.UTF8);
        }

        public BinaryEncoder(Stream stream)
        {
            this._writer = new BinaryWriter(stream, Encoding.UTF8);
        }

        private void WriteBigEndianBytes(byte[] endianBytes)
        {
            for (int i = endianBytes.Length - 1; i >= 0; i--)
            {
                this._writer.Write(endianBytes[i]);
            }
        }

        public void WriteShort(short @short)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@short));
        }

        public void WriteInt(int @int)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@int));
        }

        public void WriteLong(long @long)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@long));
        }

        public void WriteUShort(ushort @ushort)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@ushort));
        }

        public void WriteUInt(uint @uint)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@uint));
        }

        public void WriteULong(ulong @ulong)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@ulong));
        }

        public void WriteByte(byte @byte)
        {
            this._writer.Write(@byte);
        }

        public void WriteSByte(sbyte @byte)
        {
            this._writer.Write(@byte);
        }

        public void WriteFloat(float @float)
        {
            this._writer.Write(@float);
        }

        public void WriteBoolean(bool @bool)
        {
            if (@bool)
            {
                this._writer.Write((byte)1);
            }
            else
            {
                this._writer.Write((byte)0);
            }
        }

        public void WriteChar(char @char)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@char));
        }

        public void WriteDouble(double @double)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(@double));
        }

        public void WriteSingle(float single)
        {
            this.WriteBigEndianBytes(BitConverter.GetBytes(single));
        }

        public void WriteString(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            ushort num = (ushort)bytes.Length;
            this.WriteUShort(num);
            for (int i = 0; i < (int)num; i++)
            {
                this._writer.Write(bytes[i]);
            }
        }

        public void WriteStringBytes(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            int num = bytes.Length;
            for (int i = 0; i < num; i++)
            {
                this._writer.Write(bytes[i]);
            }
        }

        public void WriteBytes(byte[] data)
        {
            this._writer.Write(data);
        }

        public void Seek(int offset)
        {
            this.Seek(offset, SeekOrigin.Begin);
        }

        public void Seek(int offset, SeekOrigin seekOrigin)
        {
            this._writer.BaseStream.Seek((long)offset, seekOrigin);
        }

        public void Clear()
        {
            this._writer = new BinaryWriter(new MemoryStream(), Encoding.UTF8);
        }

    }
}
