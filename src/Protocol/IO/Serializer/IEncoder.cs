﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Spyshark.src.Protocol.Encoder.IO.Serializer
{
    public interface IEncoder
    {
        byte[] Data
        {
            get;
        }
        long Position
        {
            get;
        }

        void WriteShort(short @short);

        void WriteInt(int @int);

        void WriteLong(long @long);

        void WriteUShort(ushort @ushort);

        void WriteUInt(uint @uint);

        void WriteULong(ulong @ulong);

        void WriteByte(byte @byte);

        void WriteSByte(sbyte @byte);

        void WriteFloat(float @float);

        void WriteBoolean(bool @bool);

        void WriteChar(char @char);

        void WriteDouble(double @double);

        void WriteSingle(float single);

        void WriteString(string str);

        void WriteStringBytes(string str);

        void WriteBytes(byte[] data);

        void Clear();

        void Seek(int offset);
    }
}
