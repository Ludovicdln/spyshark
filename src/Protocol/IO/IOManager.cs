﻿using Spyshark.src.Protocol.Core.IO.Serializer;
using Spyshark.src.Protocol.Encoder.IO.Deserializer;
using Spyshark.src.Protocol.Encoder.IO.Serializer;
using Spyshark.src.Protocol.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Protocol.IO
{
    public class IOManager<T> where T : IMessage
    {
        public static void Deserialize(T message, byte[] payload)
        {
            var decoder = new BinaryDecoder(payload);

            message.Deserialize(decoder);
        }
        public static void Serialize(T message, out IEncoder encoder)
        {
            encoder = new BinaryEncoder();

            message.Serialize(encoder);
        }
    }
}
