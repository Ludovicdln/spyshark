﻿using Spyshark.src.Network;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Text;
using System.Xml.Linq;

namespace Spyshark.src.Protocol.Encoder.IO.Deserializer
{
    public class BinaryDecoder : IDecoder
    {
        private BinaryReader _reader;
        public long BytesAvailable
        {
            get
            {
                return this._reader.BaseStream.Length - this._reader.BaseStream.Position;
            }
        }
        public long Position
        {
            get
            {
                return this._reader.BaseStream.Position;
            }
        }
        public Stream BaseStream
        {
            get
            {
                return this._reader.BaseStream;
            }
        }

        public BinaryDecoder()
        {
            this._reader = new BinaryReader(new MemoryStream(), Encoding.UTF8);
        }

        public BinaryDecoder(Stream stream)
        {
            this._reader = new BinaryReader(stream, Encoding.UTF8);
        }

        public BinaryDecoder(byte[] payload)
        {
            this._reader = new BinaryReader(new MemoryStream(payload), Encoding.UTF8);
        }

        private byte[] ReadBigEndianBytes(int count)
        {
            byte[] array = new byte[count];
            for (int i = count - 1; i >= 0; i--)
            {
                array[i] = (byte)this.BaseStream.ReadByte();
            }
            return array;
        }

        public short ReadShort()
        {
            return BitConverter.ToInt16(this.ReadBigEndianBytes(2), 0);
        }

        public int ReadInt()
        {
            return BitConverter.ToInt32(this.ReadBigEndianBytes(4), 0);
        }

        public long ReadLong()
        {
            return BitConverter.ToInt64(this.ReadBigEndianBytes(8), 0);
        }

        public float ReadFloat()
        {
            return BitConverter.ToSingle(this.ReadBigEndianBytes(4), 0);
        }

        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(this.ReadBigEndianBytes(2), 0);
        }

        public uint ReadUInt()
        {
            return BitConverter.ToUInt32(this.ReadBigEndianBytes(4), 0);
        }

        public ulong ReadULong()
        {
            return BitConverter.ToUInt64(this.ReadBigEndianBytes(8), 0);
        }

        public byte ReadByte()
        {
            return this._reader.ReadByte();
        }

        public sbyte ReadSByte()
        {
            return this._reader.ReadSByte();
        }

        public byte[] ReadBytes(int n)
        {
            return this._reader.ReadBytes(n);
        }

        public BinaryDecoder ReadBytesInNewBigEndianReader(int n)
        {
            return new BinaryDecoder(this._reader.ReadBytes(n));
        }

        public bool ReadBoolean()
        {
            return this._reader.ReadByte() == 1;
        }

        public char ReadChar()
        {
            return (char)this.ReadUShort();
        }

        public double ReadDouble()
        {
            return BitConverter.ToDouble(this.ReadBigEndianBytes(8), 0);
        }

        public float ReadSingle()
        {
            return BitConverter.ToSingle(this.ReadBigEndianBytes(4), 0);
        }

        public string ReadString()
        {
            ushort n = this.ReadUShort();
            byte[] bytes = this.ReadBytes((int)n);
            return Encoding.UTF8.GetString(bytes);
        }

        public string ReadStringBitLength()
        {
            int n = this.ReadInt();
            byte[] bytes = this.ReadBytes(n);
            return Encoding.UTF8.GetString(bytes);
        }

        public string ReadStringBytes(ushort len)
        {
            byte[] bytes = this.ReadBytes((int)len);
            return Encoding.UTF8.GetString(bytes);
        }

        public void SkipBytes(int n)
        {
            for (int i = 0; i < n; i++)
            {
                this._reader.ReadByte();
            }
        }
    }
}
