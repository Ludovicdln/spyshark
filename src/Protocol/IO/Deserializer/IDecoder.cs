﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Spyshark.src.Protocol.Encoder.IO.Deserializer
{
    public interface IDecoder
    {
        long Position
        {
            get;
        }
        long BytesAvailable
        {
            get;
        }

        short ReadShort();

        int ReadInt();

        long ReadLong();

        ushort ReadUShort();

        uint ReadUInt();

        ulong ReadULong();

        byte ReadByte();

        sbyte ReadSByte();

        byte[] ReadBytes(int n);

        bool ReadBoolean();

        char ReadChar();

        double ReadDouble();

        float ReadFloat();

        string ReadString();

        string ReadStringBytes(ushort len);
    }
}
