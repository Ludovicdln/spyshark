﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Services
{
    public class BLogger
    {
        private readonly ILogger _logger;
        public BLogger(ILogger<BLogger> logger)
        {
            this._logger = logger;
        }
        public void LogInformation(string message)
        {
            _logger.LogInformation(message);
        }

        public void LogWarning(string message)
        {
            _logger.LogWarning(message);
        }

        public void LogError(string message)
        {
            _logger.LogError(message);
        }
    }
}
