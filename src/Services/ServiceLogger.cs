﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Spyshark.src.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spyshark.src.Services
{
    public class ServiceLogger
    {
        private ServiceCollection _serviceCollection;
        public ServiceLogger()
        {
            _serviceCollection = new ServiceCollection();
            this.ConfigureServices(_serviceCollection);
        }
        public BLogger GetLogger()
        {
            var serviceProvider = _serviceCollection.BuildServiceProvider();
            return serviceProvider.GetService<BLogger>();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole())
                    .AddTransient<BLogger>();
        }
    }
}
