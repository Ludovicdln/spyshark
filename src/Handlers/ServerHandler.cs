﻿using Spyshark.src.Network;
using Spyshark.src.Protocol.Frames;
using Spyshark.src.Protocol.Messages;
using Spyshark.src.Protocol.Messages.Servers;
using Spyshark.src.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Handlers
{
    public class ServerHandler : IFrameHandler
    {
        private ServiceLogger _service = new ServiceLogger();
        public ValueTask<bool> Handle(Client connection, IFrame frame)
        {
            bool result = true;    

            _service.GetLogger().LogInformation($"SEND ~ SERVER Frame Id {frame.Id} Length {frame.Payload.Length}");

            connection.WriteFrame(frame);

            return new ValueTask<bool>(result);
        }
    }
}
