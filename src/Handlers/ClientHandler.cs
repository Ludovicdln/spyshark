﻿using Spyshark.src.Network;
using Spyshark.src.Protocol.Frames;
using Spyshark.src.Protocol.Messages;
using Spyshark.src.Protocol.Messages.Servers;
using Spyshark.src.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Handlers
{
    public class ClientHandler : IFrameHandler
    {
        private ServiceLogger _service = new ServiceLogger();
        public ValueTask<bool> Handle(Client connection, IFrame frame)
        {
            bool result = true;

            if (frame.Id == (int)MessagesEnum.SELECTED_SERVER_MESSAGE)
            {
                frame = FrameManager<SelectedServerDataMessage>.ModifyFrameForWorld(frame);
                result = false;
            }

            _service.GetLogger().LogInformation($"SEND ~ CLIENT Frame Id {frame.Id} Length {frame.Payload.Length}");

            connection.WriteFrame(frame);

            return new ValueTask<bool>(result);
        }
    }
}
