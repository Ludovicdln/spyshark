﻿using Spyshark.src.Network;
using Spyshark.src.Protocol.Frames;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spyshark.src.Handlers
{
    public interface IFrameHandler
    {
        ValueTask<bool> Handle(Client connection, IFrame frame);
    }
}
