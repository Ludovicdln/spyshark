﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Pipelines.Sockets.Unofficial;
using Spyshark.src.Network;
using System;
using System.Buffers;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Spyshark
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Title = "Spyshark";

            ShowLogo();

            SocketListener listenerAuth = ListenerManager.Create(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8000), new IPEndPoint(IPAddress.Parse("127.0.0.1"), 443));

            SocketListener listenerWorld = ListenerManager.Create(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8001), new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5556));

            await listenerAuth.Run();

            await listenerWorld.Run();
        }

        private static void ShowLogo()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            string logo = @"                             _________                   .__                  __    
                            /   _____/_____ ___.__. _____|  |__ _____ _______|  | __
                            \_____  \\____ <   |  |/  ___/  |  \\__  \\_  __ \  |/ /
                            /        \  |_> >___  |\___ \|   Y  \/ __ \|  | \/    < 
                           /_______  /   __// ____/____  >___|  (____  /__|  |__|_ \
                                   \/|__|   \/         \/     \/     \/           \/";

            int totalWidth = (Console.BufferWidth + logo.Length) / 2;
            Console.WriteLine(logo.PadLeft(totalWidth) + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>

            Host.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
            {
                services.AddLogging(configure => configure.ClearProviders()
                                                         .AddConsole()
                );

            });

    }
}
